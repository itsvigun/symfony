<?php

namespace Igor\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function exampleAction($name)
    {
        return $this->render('IgorForumBundle:Default:index.html.twig', array('name' => $name));
    }
    
    public function homeAction()
    {
        return $this->render('IgorForumBundle:Home:index.html.twig');
    }
    
    public function secureAction()
    {
        $user = $this->getUser();
        if(is_null($user)){
            return $this->render('IgorForumBundle:Home:not_registered.html.twig');
        }
        $name = $user->getUsername();
        return $this->render('IgorForumBundle:Home:secure.html.twig', array('name' => $name));
    }
}
